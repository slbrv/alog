/*
 * Дано число N < 106 и последовательность пар целых чисел из [-231, 231] длиной N.
 * Построить декартово дерево из N узлов, характеризующихся парами чисел (Xi, Yi).
 * Каждая пара чисел (Xi, Yi) определяет ключ Xi и приоритет Yi в декартовом дереве.
 * Добавление узла в декартово дерево выполняйте следующим образом:
 * При добавлении узла (x, y) выполняйте спуск по ключу до узла P с меньшим приоритетом.
 * Затем разбейте найденное поддерево по ключу x так, чтобы в первом поддереве все ключи меньше x,
 * а во втором больше или равны x. Получившиеся два дерева сделайте дочерними для нового узла (x, y).
 * Новый узел вставьте на место узла P.
 *
 * Построить также наивное дерево поиска по ключам Xi.
 * Т.е., при добавлении очередного числа K в наивное дерево с корнем _root, если _root→Key ≤ K,
 * то узел K добавляется в правое поддерево _root; иначе в левое поддерево _root.
 *
 * Вычислить разницу глубин наивного дерева поиска и декартового дерева.
 *  Разница может быть отрицательна, необходимо вывести модуль разности.
 */

#include <iostream>
#include <stack>
#include <queue>

template<typename T>
class Tree
{
public:
    Tree()
        : _root(nullptr)
    {
    }
    void push(const T& value)
    {
        if (!_root)
        {
            _root = new TreeNode(value);
            return;
        }
        TreeNode* ptr = _root;

        bool find = true;
        while (find)
        {
            if (_comp(value, ptr->key))
            {
                if (!ptr->left)
                {
                    ptr->left = new TreeNode(value);
                    find = false;
                }
                else
                    ptr = ptr->left;
            }
            else
            {
                if (!ptr->right)
                {
                    ptr->right = new TreeNode(value);
                    find = false;
                }
                else
                    ptr = ptr->right;
            }
        }
    }
    int depth()
    {
        if (!_root) return 0;

        std::stack<TreeNode*> stack;
        std::stack<int> depths;

        int depth = 0;
        int max_depth = 0;

        TreeNode* ptr = _root;

        while (true)
        {
            ++depth;

            if (ptr->left)
            {
                if (ptr->right)
                {
                    stack.push(ptr->right);
                    depths.push(depth);
                }
                ptr = ptr->left;
            }
            else if (ptr->right)
            {
                ptr = ptr->right;
            }
            else
            {
                if (depth > max_depth) max_depth = depth;

                if (stack.empty()) break;

                ptr = stack.top();
                stack.pop();

                depth = depths.top();
                depths.pop();
            }
        }

        return max_depth;
    }
    ~Tree()
    {
        std::stack<TreeNode*> disp;
        disp.push(_root);
        TreeNode* ptr;
        while (!disp.empty())
        {
            ptr = disp.top();
            disp.pop();
            TreeNode* left_ptr = ptr->left;
            TreeNode* right_ptr = ptr->right;
            if (left_ptr)
                disp.push(left_ptr);
            if (right_ptr)
                disp.push(right_ptr);
            delete ptr;
        }
    }
private:
    struct TreeNode
    {
        T key;
        TreeNode* left = nullptr;
        TreeNode* right = nullptr;
        explicit TreeNode(T key)
            :
            key(key),
            left(nullptr),
            right(nullptr)
        {
        }
    };

    TreeNode* _root;
};

template<typename T>
class Treap
{
public:
    Treap()
        : _root(nullptr)
    {
    }

    void push(const T& value, const int& weight)
    {
        auto* node = new TreapNode(value, weight);
        _root = !_root ? node : push(_root, node);
    }

    int length()
    {
        std::queue<TreapNode*> uncounted;
        uncounted.push(_root);
        TreapNode* ptr;
        int idx = 1;
        int next_idx = 0;
        int len = 0;
        while (!uncounted.empty())
        {
            ptr = uncounted.front();
            uncounted.pop();
            --idx;
            if (ptr->left != nullptr)
            {
                uncounted.push(ptr->left);
                ++next_idx;
            }
            if (ptr->right != nullptr)
            {
                uncounted.push(ptr->right);
                ++next_idx;
            }
            if (idx == 0)
            {
                if (next_idx > len)
                    len = next_idx;
                idx = next_idx;
                next_idx = 0;
            }
        }
        return len;
    }

    ~Treap()
    {
        std::stack<TreapNode*> disp;
        disp.push(_root);
        TreapNode* ptr;
        while (!disp.empty())
        {
            ptr = disp.top();
            disp.pop();
            TreapNode* left_ptr = ptr->left;
            TreapNode* right_ptr = ptr->right;
            if (left_ptr)
                disp.push(left_ptr);
            if (right_ptr)
                disp.push(right_ptr);
            delete ptr;
        }
    }
private:
    struct TreapNode
    {
        T key;
        int weight;
        TreapNode* left;
        TreapNode* right;
        explicit TreapNode(T key, int weight)
            :
            key(key),
            weight(weight),
            left(nullptr),
            right(nullptr)
        {
        }
    };
    void split(TreapNode* ptr, const int& element, TreapNode*& left, TreapNode*& right)
    {
        if (ptr == nullptr)
        {
            left = nullptr;
            right = nullptr;
        }
        else
        {
            if (element <= ptr->key)
            {
                split(ptr->left, element, left, ptr->left);
                right = ptr;
            }
            else
            {
                split(ptr->right, element, ptr->right, right);
                left = ptr;
            }
        }
    }
    TreapNode* push(TreapNode*& ptr, TreapNode*& node)
    {
        if (ptr == nullptr)
        {
            return node;
        }
        if (ptr->weight > node->weight)
        {
            node->key < ptr->key ? ptr->left = push(ptr->left, node) : ptr->right =
                                                                           push(ptr->right, node);
            return ptr;
        }
        else
        {
            TreapNode* left = nullptr;
            TreapNode* right = nullptr;
            split(ptr, node->key, left, right);
            node->left = left;
            node->right = right;
            return node;
        }
    }

    TreapNode* _root;
};

int main()
{
    Treap<int> treap;
    Tree<int> tree;
    int N, x, y;
    std::cin >> N;
    for (int i = 0; i < N; ++i)
    {
        std::cin >> x >> y;
        treap.push(x, y);
        tree.push(x);
    }
    std::cout << treap.length() - tree.depth();
    return 0;
}