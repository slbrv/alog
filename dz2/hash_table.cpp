/*
 * Реализуйте структуру данных типа “множество строк” на основе динамической хеш-таблицы с открытой адресацией.
 * Хранимые строки непустые и состоят из строчных латинских букв.
 *
 * Хеш-функция строки должна быть реализована с помощью вычисления значения многочлена методом Горнера.
 *
 * Начальный размер таблицы должен быть равным 8-ми. Перехеширование выполняйте при добавлении элементов в случае,
 * когда коэффициент заполнения таблицы достигает 3/4.
 *
 * Структура данных должна поддерживать операции добавления строки в множество, удаления строки из множества и проверки
 * принадлежности данной строки множеству.
 *
 * Для разрешения коллизий используйте двойное хеширование.
*/


#include <iostream>
#include <string>
#include <vector>

class HashTable
{
public:
    HashTable()
        :
        _index(0),
        _size(8),
        _table()
    {
        _table.resize(8, "0");
    }

    inline bool push(std::string&& str)
    {
        int hash1 = hornerHash1(str);
        if (_table[hash1] == "0")//Если ячейка свободна
        {
            _table[hash1] = str;//Заполняем ее
            ++_index;

            if ((static_cast<float>(_index) / _size) > DELTA)//При переполнении
                rehash();

            return true;
        }

        if (_table[hash1] == str)
            return false;

        int del_index = -1;

        if (_table[hash1] == "1")//Находим индекс удаленной ячейки
            del_index = hash1;

        int hash2 = hornerHash2(str);
        for (int i = 1; i < _size; ++i)
        {
            std::string temp = _table[(hash1 + hash2 * i) % _size];
            if (del_index == -1 && temp == "1")
                del_index = (hash1 + hash2 * i) % _size;

            if (temp == "0")//Если ячейчка пустая
            {
                if (del_index >= 0)
                    _table[del_index] = str;
                else
                    temp = str;
                ++_index;
                if ((static_cast<float>((_index)) / _size) > DELTA)
                    rehash();
                return true;
            }

            if (temp == str)
                return false;
        }

        if (del_index >= 0)
        {
            _table[del_index] = str;
            ++_index;

            if ((static_cast<float>((_index)) / _size) > DELTA)//Смотрим кэф. заполнения
                rehash();

            return true;
        }
        return false;
    }

    inline bool remove(std::string&& str)
    {
        int index = find(std::move(str));
        if (index >= 0)
        {
            _table[index] = "1";//Указываем, что хеш удален
            --_index;
            return true;
        }
        return false;
    }

    inline int find(std::string&& str) const
    {
        int hash1 = hornerHash1(str);

        std::string temp = _table[hash1];
        if (temp == str)
            return hash1;
        if (temp == "0")
            return -1;

        int hash2 = hornerHash2(str);//Вычисляем хеш
        for (int i = 1; i < _size; ++i)
        {
            int hash = (hash1 + hash2 * i) % _size;
            temp = _table[hash];

            if (temp == "0")//Не найдено
                return -1;

            if (temp == str)//Найдено
                return hash;
        }
        return -1;
    }

private:
    const float DELTA = (2.0f / 3.0f);

    const int HASH1 = 64;
    const int HASH2 = 128;

    inline void rehash()
    {
        std::vector<std::string> temp(std::move(_table));
        int temp_size = _size;

        _size *= 2;
        _table.resize(_size, "0");//Увеличиваем в два раза и заполняем свободными местами

        for (int i = 0; i < temp_size; ++i)
        {
            std::string temp_str = temp[i];
            if (temp_str != "0" && temp_str != "1")//Освобождаем новую таблицу от удаленных хешей
                push(std::move(temp_str));
        }
    }

    //Вычисление хеш-функции строки методом Горнера
    inline int hornerHash1(const std::string& str) const
    {
        int idx = 0;

        for (char ch : str)
            idx = ((ch + idx * HASH1) % _size);//Вычисляем

        return idx;
    }

    //Вспомогательный метод вычисления хеш-функции строки методом Горнера
    //Возвращает только нечетные числа
    inline int hornerHash2(const std::string& str) const
    {
        int idx = 0;
        for (char ch : str)
            idx = ((ch + idx * HASH2) % _size);//Вычисляем

        while (idx % 2 == 0)
            idx /= 2;

        return idx;
    }

    int _index;
    int _size;

    std::vector<std::string> _table;
};

int main()
{
    HashTable table;
    char cmd = 0;
    bool status = false;
    std::string str;

    while (std::cin >> cmd >> str)
    {
        switch (cmd)
        {
        case '+':
            status = table.push(std::move(str));
            break;
        case '-':
            status = table.remove(std::move(str));
            break;
        case '?':
            status = table.find(std::move(str)) >= 0;
            break;
        default:
            std::cout << "Unknown command" << std::endl;
            break;
        }
        if (status) std::cout << "OK" << std::endl;
        else std::cout << "FAIL" << std::endl;
    }
    return 0;
}