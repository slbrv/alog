/*
 * Дано число N ≤ 10^4 и последовательность целых чисел из [-2^31..2^31] длиной N.
 * Требуется построить бинарное дерево, заданное наивным порядком вставки.
 * Т.е., при добавлении очередного числа K в дерево с корнем root,
 * если root→Key ≤ K, то узел K добавляется в правое поддерево root;
 * иначе в левое поддерево root. Выведите элементы в порядке post-order (снизу вверх).
 * In: 10 7 2 10 8 5 3 6 4 1 9
 * Out: 1 4 3 6 5 2 9 8 10 7
 * Рекурсия запрещена.
 */


#include <iostream>
#include <functional>

using namespace std;


template<typename T>
struct Node
{
    T _value;
    Node* _left;
    Node* _right;

    Node(const T& value)
        : _value(value), _left(nullptr), _right(nullptr)
    {
    }

    ~Node()
    {
        delete _left;
        delete _right;
    }
};

template<typename T, class Comparator>
class Tree
{
public:
    Tree(Comparator comp)
        : _root(nullptr),
        _comp(std::move(comp))
    {
    }
    ~Tree()
    {
        delete _root;
    }

    void push(const T& value)
    {
        auto* node = new Node<T>(value);

        if (!_root)
        {
            _root = node;
            return;
        }

        auto* ptr = _root;
        while (true)
        {
            if (_comp(value, ptr->_value))
            {
                if (ptr->_left)
                    ptr = ptr->_left;
                else
                {
                    ptr->_left = node;
                    break;
                }
            }
            else
            {
                if (ptr->_right)
                    ptr = ptr->_right;
                else
                {
                    ptr->_right = node;
                    break;
                }
            }
        };

    }

    void print()
    {
        postOrderPrint(_root);
    }

private:

    void postOrderPrint(const Node<T>* node)
    {
        if (!node) return;

        postOrderPrint(node->_left);
        postOrderPrint(node->_right);

        std::cout << node->_value << " ";
    }

    Node<T>* _root;
    Comparator _comp;
};

int main()
{
    int N = 0;
    cin >> N;

    std::function<bool(int, int)> func = [](int a, int b){return a < b;};

    Tree<int, std::function<bool(int, int)>> tree(func);

    int val = 0;
    for (int i = 0; i < N; ++i)
    {
        cin >> val;
        tree.push(val);
    }
    tree.print();
    return 0;
}