//Требуется отыскать самый короткий маршрут между городами.
// Из города может выходить дорога, которая возвращается в этот же город.
//
//Требуемое время работы O((N + M)log N), где N – количество городов, M – известных дорог между ними.
//N ≤ 10000, M ≤ 250000.
//Длина каждой дороги ≤ 10000.

#include <vector>
#include <list>
#include <iostream>
#include <set>
#include <assert.h>

typedef std::pair<int, int> Vertex;

const int MAX_INT = INT32_MAX;

class Graph
{

public:

    explicit Graph(int n)
        :
        _verticesCount(n),
        _vertices(n)
    {
    }

    //Добавление грани
    void addEdge(int fstVertex, int secVertex, int distance)
    {
        _vertices[fstVertex].push_back(Vertex(secVertex, distance));
        _vertices[secVertex].push_back(Vertex(fstVertex, distance));
    }

    //Нахождение кратчайшего пути
    int getShortestPath(int start, int goal)
    {
        //Массив расстояний
        std::vector<int> distances(_verticesCount, MAX_INT);
        //Массив посещенных вершин
        std::vector<bool> visited(_verticesCount, false);

        //Набор вершин
        std::set<Vertex> vertexSet;
        vertexSet.insert(Vertex(0, start));
        distances[start] = 0;

        while (!vertexSet.empty()) {
            int current = vertexSet.begin()->second;
            visited[current] = true;
            vertexSet.erase(vertexSet.begin());

            std::vector<Vertex> nearest;

            for (auto& vertex : _vertices[current])
                nearest.push_back(vertex);

            for (auto& vertex : nearest) {
                if (distances[vertex.first] > distances[current] + vertex.second) {
                    if (distances[vertex.first] != MAX_INT) {
                        vertexSet.erase(Vertex(distances[vertex.first], vertex.first));
                    }

                    distances[vertex.first] = distances[current] + vertex.second;
                    vertexSet.insert(Vertex(distances[vertex.first], vertex.first));
                }
            }
        }

        return distances[goal];
    }

private:

    //Массив списков смежности взвешенного графа
    std::vector<std::list<Vertex>> _vertices;
    int _verticesCount;
};

int main()
{
    int N = 0, M = 0;

    std::cin >> N;
    std::cin >> M;

    assert(N > 0);

    Graph graph(N);

    for (int i = 0; i < M; ++i)
    {
        int
            fstVertex = 0,
            secVertex = 0,
            distance = 0;

        std::cin >> fstVertex;
        std::cin >> secVertex;
        std::cin >> distance;

        graph.addEdge(fstVertex, secVertex, distance);
    }

    int start = 0, goal = 0;

    std::cin >> start;
    std::cin >> goal;

    std::cout << graph.getShortestPath(start, goal) << std::endl;
    return 0;
}