#include "arc_graph.h"

ArcGraph::ArcGraph(int n)
    :
    _adjPair(),
    _verticesCount(n)
{
}

ArcGraph::ArcGraph(const IGraph& graph)
{
    for (int i = 0; i < graph.VerticesCount(); ++i)
    {
        std::vector<int> nextVertices = graph.GetNextVertices(i);
        for (auto vertex : nextVertices)
        {
            _adjPair.emplace_back(i, vertex);
        }
    }
}
void ArcGraph::AddEdge(int from, int to)
{
    _adjPair.emplace_back(from, to);
}

int ArcGraph::VerticesCount() const
{
    std::vector<int> vertices = std::vector<int>(_adjPair.size());

    for (auto pair : _adjPair)
    {
        vertices[pair.first] = 1;
        vertices[pair.second] = 1;
    }

    int count = 0;

    for (int vertex : vertices)
        if (vertex == 1)
            ++count;

    return count;
}

std::vector<int> ArcGraph::GetNextVertices(int vertex) const
{
    std::vector<int> vertices;

    for (auto pair : _adjPair)
        if (pair.first == vertex)
            vertices.push_back(pair.second);

    return vertices;
}

std::vector<int> ArcGraph::GetPrevVertices(int vertex) const
{
    std::vector<int> vertices;

    for (auto pair : _adjPair)
        if (pair.second == vertex)
            vertices.push_back(pair.first);

    return vertices;
}
