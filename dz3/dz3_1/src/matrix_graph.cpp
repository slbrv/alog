#include "matrix_graph.h"

MatrixGraph::MatrixGraph(int n)
    : _adjMatrix(n)
{
    for (auto & i : _adjMatrix)
        for (unsigned long j = 0; j < _adjMatrix.size(); ++j)
            i[j] = 0;
}

MatrixGraph::MatrixGraph(const IGraph& graph)
    : _adjMatrix(graph.VerticesCount())
{
    for (int i = 0; i < graph.VerticesCount(); ++i)
    {
        std::vector<int> vertices = graph.GetNextVertices(i);
        _adjMatrix[i] = std::vector<int>(graph.VerticesCount());
        for (int vertex : vertices)
        {
            _adjMatrix[i][vertex] = 1;
        }
    }
}

void MatrixGraph::AddEdge(int from, int to)
{
    _adjMatrix[from][to] = true;
}

int MatrixGraph::VerticesCount() const
{
    _adjMatrix.size();
}

std::vector<int> MatrixGraph::GetNextVertices(int vertex) const
{
    std::vector<int> vertices;

    for (unsigned long j = 0; j < _adjMatrix.size(); ++j)
        if (_adjMatrix[vertex][j] == 1)
            vertices.push_back(j);

    return vertices;
}

std::vector<int> MatrixGraph::GetPrevVertices(int vertex) const
{
    std::vector<int> vertices;

    for (unsigned long i = 0; i < _adjMatrix.size(); ++i)
        if (_adjMatrix[i][vertex] == 1)
            vertices.push_back(i);

    return vertices;
}
