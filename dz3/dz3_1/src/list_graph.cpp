#include "list_graph.h"

ListGraph::ListGraph(int n)
    : _adjList(),
    _verticesCount(n)
{
}

ListGraph::ListGraph(const IGraph& graph)
    : _adjList(graph.VerticesCount())
{
    for (int from = 0; from < _verticesCount; ++from) {
        std::vector<int> nearest = graph.GetNextVertices(from);

        for (int to : nearest) {
            AddEdge(from, to);
        }
    }
}

void ListGraph::AddEdge(int from, int to)
{
    _adjList[from].push_back(to);
}

int ListGraph::VerticesCount() const
{
    return _adjList.size();
}

std::vector<int> ListGraph::GetNextVertices(int vertex) const
{
    std::vector<int> vertices;

    for (int i : _adjList[vertex])
        vertices.push_back(i);

    return vertices;
}

std::vector<int> ListGraph::GetPrevVertices(int vertex) const
{
    std::vector<int> vertices;

    for (int from = 0; from < _adjList.size(); from++) {
        for (int to : _adjList[from]) {
            if (to == vertex) {
                vertices.push_back(from);
                break;
            }
        }
    }

    return vertices;
}
