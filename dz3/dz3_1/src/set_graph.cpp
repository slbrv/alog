#include "set_graph.h"

SetGraph::SetGraph(int count)
    : _adjSet(count)
{
}

SetGraph::SetGraph(const IGraph& graph)
    : _adjSet(graph.VerticesCount())
{
    for (int from = 0; from < _verticesCount; ++from) {
        std::vector<int> nextVertices = graph.GetNextVertices(from);
        for (int to : nextVertices) {
            AddEdge(from, to);
        }
    }
}

void SetGraph::AddEdge(int from, int to)
{
    _adjSet[from].insert(to);
    _adjSet[to].insert(from);
}

int SetGraph::VerticesCount() const
{
    return _adjSet.size();
}

std::vector<int> SetGraph::GetNextVertices(int vertex) const
{
    std::vector<int> vertices;

    for (int i = 0; i < _adjSet.size(); ++i)
        if (_adjSet[vertex].find(i) != _adjSet[vertex].end())
            vertices.push_back(i);

    return vertices;
}

std::vector<int> SetGraph::GetPrevVertices(int vertex) const
{
    std::vector<int> vertices;

    for (int i = 0; i < _adjSet.size(); ++i)
        if (_adjSet[i].find(vertex) != _adjSet[i].end())
            vertices.push_back(i);

    return vertices;
}
