#ifndef LIST_GRAPH_H
#define LIST_GRAPH_H

#include "graph.h"
#include <list>

class ListGraph : public IGraph
{
public:
    ListGraph(int n);
    ListGraph(const IGraph& graph);

    void AddEdge(int from, int to) override;

    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::list<int>> _adjList;
    int _verticesCount;
};

#endif //LIST_GRAPH_H
