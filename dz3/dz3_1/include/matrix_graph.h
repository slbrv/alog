#ifndef MATRIX_GRAPH_H
#define MATRIX_GRAPH_H

#include "graph.h"

class MatrixGraph : public IGraph
{
public:
    MatrixGraph(int n);
    MatrixGraph(const IGraph& graph);

    void AddEdge(int from, int to) override;

    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::vector<int>> _adjMatrix;
    int _verticesCount;
};

#endif //MATRIX_GRAPH_H
