#ifndef SET_GRAPH_H
#define SET_GRAPH_H

#include "graph.h"
#include <unordered_set>

class SetGraph : public IGraph
{
public:
    SetGraph(int n);
    SetGraph(const IGraph& graph);

    void AddEdge(int from, int to) override;

    int VerticesCount() const override;

    std::vector<int> GetNextVertices(int vertex) const override;
    std::vector<int> GetPrevVertices(int vertex) const override;

private:
    std::vector<std::unordered_set<int>> _adjSet;
    int _verticesCount;
};

#endif //SET_GRAPH_H
