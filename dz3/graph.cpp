//Дан невзвешенный неориентированный граф.
//В графе может быть несколько кратчайших путей между какими-то вершинами.
//Найдите количество различных кратчайших путей между заданными вершинами.

#include <vector>
#include <queue>
#include <list>
#include <iostream>
#include <assert.h>

const int MAX_INT = INT32_MAX;

struct Vertex
{
    enum State
    {
        UNVISITED,
        VISITED
    };

    int _parent = -1;
    int _state = UNVISITED;

    int _distance = 0;
    int _pathCount = 0;
};

class Graph
{

public:

    explicit Graph(int n)
        :
        _verticesCount(n),
        _vertices(n)
    {
    }

    //Добавление грани
    void addEdge(int fstVertex, int secVertex)
    {
        _vertices[fstVertex].push_back(secVertex);
        _vertices[secVertex].push_back(fstVertex);
    }

    //Поиск в ширину
    int getPathCount(int start, int goal)
    {
        std::queue<int> queue;
        queue.push(start);

        std::list<int> verticesList;

        int shortestPath = MAX_INT;

        //Список для обхода в ширину
        std::vector<Vertex> bfsVector(_vertices.size());

        // Количество уникальных путей
        bfsVector[start]._pathCount = 1;

        while (!queue.empty())
        {
            // Вынимаем передний элемент из очереди
            int front = queue.front();
            queue.pop();

            verticesList = _vertices[front];

            for (int i : verticesList)
            {
                if (i != bfsVector[front]._parent)
                {
                    //Если вершина непосещена, то ставим ее в очередь
                    if (bfsVector[i]._state == Vertex::UNVISITED)
                    {
                        queue.push(i);
                        bfsVector[i]._parent = front;
                        //Увеличим расстояние
                        bfsVector[i]._distance = bfsVector[front]._distance + 1;
                        //Отмечаем вершину как посещенную
                        bfsVector[i]._state = Vertex::VISITED;
                    }

                    //Если кратчайший путь еще не найден
                    if(shortestPath == MAX_INT)
                    {
                        //Если пришли в конечную вершину, то берем ее как кратчайшую
                        shortestPath = i != goal ? shortestPath : bfsVector[goal]._distance;
                    }
                    //Если найденный путь длиннее найденного кратчайшего, то завершаем поиск
                    else if(bfsVector[i]._distance > shortestPath) break;

                    //Находим сумму путей данной вершины и ее родителя
                    if (bfsVector[i]._distance - 1 == bfsVector[front]._distance)
                        bfsVector[i]._pathCount += bfsVector[front]._pathCount;
                }
            }
        }

        return bfsVector[goal]._pathCount;
    }

private:

    //Массив списков смежности
    std::vector<std::list<int>> _vertices;
    int _verticesCount;
};

int main()
{
    int V = 0, N = 0;

    std::cin >> V;
    std::cin >> N;

    assert(V > 0);

    Graph graph(V);

    for (int i = 0; i < N; ++i)
    {
        int fstVertex, secVertex;

        std::cin >> fstVertex;
        std::cin >> secVertex;

        graph.addEdge(fstVertex, secVertex);
    }

    int start = 0, goal = 0;

    std::cin >> start;
    std::cin >> goal;

    std::cout << graph.getPathCount(start, goal) << std::endl;
    return 0;
}